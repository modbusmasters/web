var express = require('express');
var expressValidator = require('express-validator');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jwt = require('express-jwt');

var config = require('./config');

// Authentication check with Auth0
var jwtCheck = jwt({
  secret: new Buffer(config['auth0Secret'], 'base64'),
  audience: config['auth0Audience']
});

// Mongoose mongodb
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/testDb');

var createSampleData = require('./routes/createMasterSlave');
var api = require('./routes/api/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(expressValidator());

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/createSampleData', createSampleData);
app.use('/api', jwtCheck);
app.use('/api', api);

// Catch all route for api pages.
app.get('/api/*', function(req, res) {
  res.sendStatus(404);
});

// Catch all route for displaying Vue routes.
app.get('*', function (req, res) {
     res.render('default');
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
