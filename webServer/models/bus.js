var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TempLog = require('./tempLog');

var busSchema = new Schema({
    _slave: { type: Schema.Types.ObjectId, ref: 'slave' },
    registrationNumber: String,
    tempQuotient: Number,
    tempMin: Number,
    scheduleStart: String,
    scheduleStop: String
});

module.exports =  mongoose.model('bus', busSchema);