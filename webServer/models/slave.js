var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Bus = require('./bus');

var slaveSchema = new Schema({
    deviceId: Number,
    _bus: { type: Schema.Types.ObjectId, ref: 'bus' }
});

module.exports = mongoose.model('slave', slaveSchema);