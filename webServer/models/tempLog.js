var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tempLogSchema = new Schema({
    _bus: { type: Schema.Types.ObjectId, ref: 'bus' },
    dateTime: Date,
    inTemp: Number,
    returnTemp: Number
});

module.exports = mongoose.model('tempLog', tempLogSchema);