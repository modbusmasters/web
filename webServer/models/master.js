var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Slave = require('./slave');

var masterSchema = new Schema({
    _id: Number,
    serialNumber: String,
    name: String,
    description: String,
    online: Boolean,
    slaves: [Slave.schema]
});

module.exports = mongoose.model('master', masterSchema);