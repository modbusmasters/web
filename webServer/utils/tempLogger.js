"use strict";

var express = require('express');
var mongoose = require('mongoose');

var Master = require('../models/master'); 
var Bus = require('../models/bus'); 
var TempLog = require('../models/tempLog');
var elapsedTime = require('../utils/elapsedTime');

/**
 * Handles the process of adding new temperature logs to the database. 
 * Mainly used by socketServer.
 * @module utils/tempLogger
 * @see module:socketServer
 */
class TempLogger {
    
    constructor(masterId, slaveDeviceId) {
        this.masterId = masterId;
        this.slaveDeviceId = slaveDeviceId;
        this.busId = null;
    }
    
    push(inTemp, returnTemp, cb) {
        if(typeof(cb) !== typeof(Function)) cb(false);
        
        var _this = this;
        
        var start = process.hrtime(); //only for debug
        
        // Find Slave
        Master.find({ _id: this.masterId, 'slaves.deviceId': this.slaveDeviceId }, function(err, masters) {
            //elapsedTime(start, 'to fetch master');
            if(err || !masters | masters.length < 1) return cb(false);

            var slaves = masters[0].slaves.toObject();
            
            _this.busId = slaves.find(s => s.deviceId == _this.slaveDeviceId)._bus;
            
            //Get bus
            Bus.findById(_this.busId, function(err, bus) {
                //elapsedTime(start, 'until finding bus');
                if(err) return cb(false);
                if(!bus) {
                    console.log('Bus not found for slave');
                    return cb(false);
                }
               
                // Add temp log to bus
                var dateTime = new Date();
                
                var tLog = new TempLog({
                    _bus: bus._id,
                    dateTime: dateTime,
                    inTemp: inTemp,
                    returnTemp: returnTemp 
                });
                
                // Add templog
                tLog.save(function(err) {
                    if(err) {
                        console.log('Temp log could not be saved to db.');
                        return cb(false);
                    }

                    //elapsedTime(start, 'to completion');
                    
                    return cb(true);
                
                });
                
            });
        });
    }
}

module.exports = TempLogger;