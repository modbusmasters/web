/** 
 * Contains the necessary functionality for communication
 * with our custom modbus masters.
 * 
 * @module utils/modbusMasterCom
 * @see class:ModbusMasterCom
 */

"use strict";

/** 
 * Contains the necessary functionality for communication
 * with our custom modbus masters.
 */
class ModbusMasterCom {
    
    /**
     * Creates an instance of ModbusMasterCom.
     */
    constructor() {
        
        this.commands = {
            addSlave: 0x01,
            deleteSlave: 0x02,
            setSlaveData: 0x04,
            setMasterData: 0x08,
            initiateStream: 0x10,
            stopStream: 0x20,
            sendSlaveData: 0x40,
            sendMasterId: 0x80
        }
    }

    /**
     * Extracts and returns the command from a data package.
     * @param {Array<byte>} data - TCP data package.
     * @return {number} - The command code.
     */      
    getCmdType(data) {
        return data[3];
    }
    
    getMasterId(data) {
        var masterIdArr = new Uint8Array(2);
        masterIdArr[0] = data[1];
        masterIdArr[2] = data[2];
        
        return this.bytesToInt16(masterIdArr);
    }
    
    /**
     * Extracts and returns the payload from a data package
     * @param {Array<byte>} data - TCP data package.
     * @return {Array<byte>} The payload.
     */      
    getPayload(data) {
        var cmd = this.getCmdType(data);
        
        var dataLen;
        
        switch(cmd) {
            case this.commands.addSlave:
                dataLen = 7;
                break;
            case this.commands.deleteSlave:
                dataLen = 1;
                break;
            case this.commands.setSlaveData:
                dataLen = 15;
                break;
            case this.commands.setMasterData: 
                dataLen = 4;
                break;
            case this.commands.initiateStream: 
                dataLen = 1;
                break;
            case this.commands.stopStream: 
                dataLen = 1;
                break;
            case this.commands.sendSlaveData:
                dataLen = 12;
                break;
            case this.commands.sendMasterId:
                dataLen = 2;
                break;
        }
        
        var payloadStart = data.length - dataLen - 1;
        var payloadLen = data.length - payloadStart - 1;
        
        // console.log('Payload start: ' + payloadStart);
        // console.log('Payload length: ' + payloadLen);
        
        var payload = new Uint8Array(payloadLen);
        
        var j = 0;
        for(let i = payloadStart; i < data.length - 1; i++) {
            payload[j] = data[i];
            j++;
        }
        
        // console.log('Payload:');
        // console.log(payload);

        return payload;
    }
    
    /**
     * @param {Object} socket - Node net.Socket connection to send package to.
     * @param {number} address - Modbus master address.    
     * @param {number} slaveId - The ID of the new slave.
     * @param {string} regNr - The registration number of the bus connected to the slave. Can be left empty.
     */    
    addSlave(socket, address, slaveId, regNr) {
        var payload = new Uint8Array(7);
        payload[0] = slaveId;
        
        if(!regNr) regNr = '';
        
        if(regNr.length > 6) regNr.length = 6;
        
        if(regNr.length > 0) {
            for(var i = 0; i < regNr.length; i++) {
               payload[i+1] = regNr.charCodeAt(i); 
            }
        }
        else {
            for(var i = 1; i <= 6; i++) {
                payload[i] = '*'.charCodeAt(0);
            }
        }
        
        this.sendPackage(socket, address, this.commands.addSlave, payload);
    }
    
    /**
     * @param {Object} socket - Node net.Socket connection to send package to.
     * @param {number} address - Modbus master address.    
     * @param {number} slaveId - The ID of the slave that will be removed.
     */    
    deleteSlave(socket, address, slaveId) {
        var payload = new Uint8Array(1);
        payload[0] = slaveId;
        
        this.sendPackage(socket, address, this.commands.deleteSlave, payload);
    }
    
    /**
     * @param {Object} socket - Node net.Socket connection to send package to.
     * @param {number} address - Modbus master address.    
     * @param {number} slaveId - The ID of the slave to start streaming data from.
     */    
    initiateStream(socket, address, slaveId) {
        var payload = new Uint8Array(1);
        payload[0] = slaveId;
        
        this.sendPackage(socket, address, this.commands.initiateStream, payload);
    }

    /**
     * @param {Object} socket - Node net.Socket connection to send package to.
     * @param {number} address - Modbus master address.    
     * @param {number} slaveId - The ID of the slave to stop streaming data from.
     */    
    stopStream(socket, address, slaveId) {
        var payload = new Uint8Array(1);
        payload[0] = slaveId;
        
        this.sendPackage(socket, address, this.commands.stopStream, payload);
    }
    
    /**
     * @param {Object} socket - Node net.Socket connection to send package to.
     * @param {number} address - Modbus master address.    
     * @param {number} time - Set current time as seconds from midnight.
     */
    setMasterData(socket, address, time) {
        var payload = new Uint8Array(4);
        
        //Convert timestamp to bytes
        for(let i = 0; i < 4; i++) {
            payload[i] = (time >> (i*8));
        }
        
        console.log('Sending server time to master ' + address + ': ' + time); 
        //console.log('Socket:');
        //console.log(socket);
        this.sendPackage(socket, address, this.commands.setMasterData, payload);
    }
    
    /**
     * @param {Object} socket - Node net.Socket connection to send package to.
     * @param {number} address - Modbus master address.
     * @param {number} slaveId - Slave ID, 0 - 255
     * @param {number} oprationMode - Decides whether the device should be in
     * automatic, manual or forced mode. 
     * @param {Array<number>} relayStates - Power states of the three relays on the slave.
     * @param {number} returnWaterTemp - Minimum return water value.
     * @param {number} tempModifier - Unique temp modifier for the connected bus.
     * @param {number} returnWaterTemp - Minimum return water temperature.
     * @param {number} scheduleStart - Number of minutes past midnight the
     * scheduled heating period will start.
     * @param {number} scheduleStop - Number of minutes past midnight the 
     * scheduled heating period will stop.
     */
    setSlaveData(socket, address, slaveId, operationMode, relayStates, returnWaterTemp, tempModifier, scheduleStart, scheduleStop) {
          
        var payload = new Uint8Array(15);
        
        payload[0] = slaveId;
        payload[1] = operationMode;
        
        // Set relayState byte
        for(let i = 0; i < relayStates.length; i++) {
            if(relayStates[i]) {
                payload[2] |= (1 << i);    
            }
        }
        
        var floatBytes = this.toFloat32ByteArray(returnWaterTemp);
        payload[3] = floatBytes;
        payload[4] = (floatBytes >> 8);;
        payload[5] = (floatBytes >> 16);
        payload[6] = (floatBytes >> 24);
        
        // Convert float to 4 bytes
        floatBytes = this.toFloat32ByteArray(tempModifier);
        payload[7] = floatBytes;
        payload[8] = (floatBytes >> 8);
        payload[9] = (floatBytes >> 16);
        payload[10] = (floatBytes >> 24);
        
        var scheduleStartBytes = this.int16ToBytes(scheduleStart);
        payload[11] = scheduleStartBytes[0];
        payload[12] = scheduleStartBytes[1];
        
        var scheduleStopBytes = this.int16ToBytes(scheduleStop);
        payload[13] = scheduleStopBytes[0];
        payload[14] = scheduleStopBytes[1];
        
        //Send package
        this.sendPackage(socket, address, this.commands.setSlaveData, payload);
        
    }
    
    /** 
     * Sends a custom package to a connected Master.
     * @param {Object} socket - Node net.Socket connection to send package to.
     * @param {number} address - Modbus master address.
     * @param {number} command - Command code, 0 - 255. 
     * @param {Uint8Array} data - Package payload.
     */
    sendPackage (socket, address, command, data) {
        
        var payloadLength = data.length;
        var packageLength = 1+2+1+1+payloadLength+1;
        
        var dataPackage = new Buffer(packageLength);
        var masterIdBytes = this.int16ToBytes(address);
        
        dataPackage[0] = 0x99;
        dataPackage[1] = masterIdBytes[0];
        dataPackage[2] = masterIdBytes[1];
        dataPackage[3] = command;
        dataPackage[4] = payloadLength;
        
        var i = 5;
        while(i < (payloadLength+5)) {
            dataPackage[i] = data[i-5];
            i++;
        }
        
        dataPackage[i] = 0xAF;

        socket.write(dataPackage);
        // console.log('Sending slave data:')
        // console.log(dataPackage);
    
    }
    
    toFloat32ByteArray(value) {
        var bytes = 0;
        switch (value) {
            case Number.POSITIVE_INFINITY: bytes = 0x7F800000; break;
            case Number.NEGATIVE_INFINITY: bytes = 0xFF800000; break;
            case +0.0: bytes = 0x40000000; break;
            case -0.0: bytes = 0xC0000000; break;
            default:
                if (Number.isNaN(value)) { bytes = 0x7FC00000; break; }
    
                if (value <= -0.0) {
                    bytes = 0x80000000;
                    value = -value;
                }
    
                var exponent = Math.floor(Math.log(value) / Math.log(2));
                var significand = ((value / Math.pow(2, exponent)) * 0x00800000) | 0;
    
                exponent += 127;
                if (exponent >= 0xFF) {
                    exponent = 0xFF;
                    significand = 0;
                } else if (exponent < 0) exponent = 0;
    
                bytes = bytes | (exponent << 23);
                bytes = bytes | (significand & ~(-1 << 23));
            break;
        }
        return bytes;
    }
    
    toFloat (n) {
        var sign = (n >> 31) * 2 + 1;
        var exp = (n >>> 23) & 0xff;
        var mantissa = n & 0x007fffff;
        if (exp === 0xff) {
            return mantissa ? NaN : sign * Infinity;
        }
        else if (exp) {
            exp -= 127;
            mantissa |= 0x00800000;
        }
        else {
            exp = -126;
        }
        return sign * mantissa * Math.pow(2, exp - 23);
    }

    /** 
     * Converts 16 bit integer to a 2 byte Uint8array. 
     * @param {Array<byte>} bytes - 2 byte array representing the 16 bit integer (low byte first).
     * @return {Uint8array} The 16 bit integer's two bytes.
     */    
    int16ToBytes(int16) {
       var b16 = new Uint16Array(1);
       b16[0] = int16;
       var b8 = new Uint8Array(b16.buffer);
       return b8;
    }

    /** 
     * Converts array with two binary numbers (low byte first) to int.
     * @param {Array<byte>} bytes - 2 byte array representing the 16 bit integer (low byte first).
     * @return {number} The 16 bit integer.
     */    
    bytesToInt16(bytes) {
        var int = bytes[1] << 8;
        int |= bytes[0];
        return int;
    }
    
    /** 
     * Converts 4 bytes to a 32 bit unsigned integer. 
     * @param {Array<byte>} bytes - Byte array containing the 16 bit integer (low byte first).
     * @param {number} [startPos=0] - Start position of integer in the array.
     * @return {number} The 16 bit unsigned integer.
     */  
    bytesToInt32(bytes, startPos) {
        if(typeof(startPos)==='undefined') startPos = 0;
        var int32;
        for(let i = 3; i >= 0; i--) {
            int32 |= bytes[i+startPos] << i*8;
        }
        return int32 >>> 0; //Convert to unsigned
    }
    
}

module.exports = new ModbusMasterCom();