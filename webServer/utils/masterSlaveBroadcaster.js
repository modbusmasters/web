'use strict';

var mongoose = require('mongoose');
var Master = require('../models/master');
var Bus = require('../models/bus');

class MasterSlaveBroadcaster {
    
    constructor(mCom) {
        this.mCom = mCom;
    }
    
    sendSlaveList(socket, masterId, cb) {
        Master
            .findById(masterId)
            .populate('slaves._bus')
            .exec((err, master) => {
                
                if(err || !master) {
                    if(err) throw err;  
                    cb(false);
                }
                
                var regNr = '';
                var i = 0;
                var sendInterval = setInterval(() => {
                    
                   if(master.slaves[i]._bus) regNr = master.slaves[i]._bus.registrationNumber;
                   this.mCom.addSlave(socket, masterId, master.slaves[i].deviceId, regNr);
                   console.log('Wrote slave ID to master');
                   i++;
                   if(i == master.slaves.length) {
                       clearInterval(sendInterval);
                   }
                }, 200);
                
                cb(true);   
            });
    }
}

module.exports = MasterSlaveBroadcaster;