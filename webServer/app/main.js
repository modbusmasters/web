import Vue from 'vue'
import VueRouter from './vue-router.min.js'
import VueResource from 'vue-resource'
import { configRouter } from './routeConfig'
import Filters from './filters'

import CustomDirectives from './directives';

// Install router
Vue.use(VueRouter)

Vue.use(VueResource);
Vue.http.options.root = '/api';

Vue.use(CustomDirectives);

// Create router
export var router = new VueRouter({
  history: true,
  saveScrollPosition: true
})

// Configure router
configRouter(router)

// Bootstrap app
const App = Vue.extend(require('./app.vue'))

//Add filters
Vue.filter('regNr', Filters.regNr);

router.start(App, 'body')

// Debug only
window.router = router
//Vue.config.debug = true;