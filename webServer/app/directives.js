export default function(Vue) {

        Vue.directive('timepicker', {
            twoWay: true,
            priority: 1000,
        
            params: ['options'],
        
            bind: function() {
                $(this.el).timepicker({ showMeridian: false });
                $(this.el).on('change', () => {
                    this.set($(this.el).val());
                });
            },
            update: function(value) {
                $(this.el).timepicker('setTime', value);
            },
            unbind: function() {
                
            }
        });    
        
        
        Vue.directive('toggle', {
            twoWay: true,
            priority: 1000,
            
            bind: function() {
                $(this.el).bootstrapToggle();
                $(this.el).on('change', () => {
                    this.set($(this.el).prop('checked'));
                });
            },
            update: function() {},
            unbind: function() {
                $(this.el).bootstrapToggle('destroy')
            }
        });

}

