export function configRouter (router) {

  router.map({

    '/home': {

      component: require('./components/home.vue')
    },
    '/masters': {

      component: require('./components/master-list.vue')
    },
    '/masters/:id/slaves': {

      component: require('./components/slave-list.vue')
    },
    
    '/slaves/:id': {

      component: require('./components/slave.vue')
    },
    
    '/settings': {

      component: require('./components/settings/index.vue')
    },
    '/settings/buses': {

      component: require('./components/settings/buses.vue')
    },
    '/settings/slaves': {

      component: require('./components/settings/slaves.vue')
    },
    '/settings/masters': {

      component: require('./components/settings/masters.vue')
    },
    
    '/stats': {

      component: require('./components/stats.vue')
    },
    
    '/users': {

      component: require('./components/users.vue')
    },
    
    '/cp': {

      component: require('./components/cp.vue')
    },

    // not found handler
    '*': {
      component: require('./components/not-found.vue')
    }

  });

  
  // redirect
  router.redirect({
    '/': '/home'
  });

  // global before
  // 3 options:
  // 1. return a boolean
  // 2. return a Promise that resolves to a boolean
  // 3. call transition.next() or transition.abort()
  // Apply user authentication for all routes except the home page
  router.beforeEach((transition) => {
    
    if (transition.to.path != '/home') {
      
      if(localStorage.getItem('userToken')) {

        console.log('User authenticated successfully');
        transition.next();
      }
      else {
        // TODO: maybe check with the server that the token is valid?
        console.log('User auth failed');
        transition.abort();
        router.go('/home');
      }
    }
    else {
      transition.next();
    }
  });
}