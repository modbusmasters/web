export class AdminTable {
    
    constructor(tableId, colIds, idCol) {
        
        // The <table> element
        this.table = $(tableId);
        
        // Array with IDs of all table columns
        this.colIds = colIds;
        
        // Array storing all table cells for the table
        this.data = [[]];
        
        // Indicates that the column at index "idCol" should be used
        // as an unique identifier for each table row. The "id" attribute
        // of the <tr> element is set to that identifier.
        this.idCol = idCol;
        
    }
    
    populate(data) {
        this.data = data;
    }
    
    // Removes deleted rows and adds new rows to the table
    refresh() {
        var tableRows = $(this.table + ' tbody tr').length;
        var dataRows = this.data.length;
        
        if(dataRows != tableRows) {
            
            for(let i = 0; i < tableRows; i++) {
                
                let trId = $(this.table).find('tbody').find('tr').eq(i);
                let dataId = this.data[i][this.idCol];
                
            }     
        }

    }

    
}