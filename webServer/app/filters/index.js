export default {
    
    regNr: function(data) {
        if(typeof data !== 'undefined' && data.length == 6) { 
            var regNr = data.substr(0,3) +  ' ' + data.substr(3,3);
            return regNr.toUpperCase();
        }
        
        return data;
    }
    
}