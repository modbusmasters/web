//TODO: Add proper error message display

var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var slavesRouter = require('./slaves');
var busesRouter = require('./buses');
var elapsedTime = require('../../utils/elapsedTime');

var Master = require('../../models/master');

router.use('/:masterId/slaves', slavesRouter);

// Fetch and display all masters from db
router.get('/', function(req, res) {
    
    Master
        .find({})
        .sort([['_id', 'ascending']])
        .exec(function(err, masters) {
            
            if(err) {
                res.sendStatus(400);
                throw err;   
            }
            
            res.json(masters);    
            
        });
});

router.get('/:id', function(req, res) {
    
    Master
        .findById(req.params.id)
        .select('-slaves')
        .exec(function(err, masters) {
            
            if(err) {
                res.sendStatus(400);
                throw err;   
            }
            
            res.json(masters);    
            
        });
});

router.put('/:masterId', function(req, res) {
    var errors = [];
    res.status(400);

    req.checkParams("masterId", "Enter a valid old master ID").isInt();
    req.checkBody("_id", "Enter a valid new master ID").isInt();
    req.checkBody("name", "Enter a valid master name").notEmpty();
    if(req.validationErrors()) {
        errors.push(req.validationErrors());
        return res.send({ "errors": errors });
    }
    
    Master.findById(req.params.masterId, function(err, master) {
        if(err || !master) {
            errors.push({msg: 'Could not find master to change'});
            return res.send({ "errors": errors });
        }
        
        console.log('Setting id to ' + req.body._id);
        master._id = req.body._id;
        master.name = req.body.name;
        
        master.save(function(err) {
            if(err) {
                errors.push({ msg: 'Server error when saving updated master to db.' });
                console.log(err);
                return res.send({ "errors": errors });             
            }
            
            res.status(200);
            return res.send({ msg: 'Success!' });
        })              
        
    });
});


router.post('/', function(req, res) {
    var startTime = process.hrtime();
    var errors = [];
    res.status(400);
    
    var master = new Master();

    
    req.checkBody("_id", "Enter a valid master ID").isInt();
    if(req.validationErrors()) {
        errors.push(req.validationErrors());
        return res.send({ "errors": errors });
    }
    
    Master.find({ _id: req.body._id }, function(err, masters) {
        if(err || masters.length > 0) {
            errors.push({ msg: 'Master ID is already in use.' });
            return res.send({ "errors": errors });            
        }
        
        master._id = req.body._id;
        master.name = req.body.name;
        
        master.save(function(err) {
            if(err) {
                errors.push({ msg: 'Server error when saving master to db.' });
                console.log(err);
                return res.send({ "errors": errors });             
            }
            
            res.status(200);
            elapsedTime(startTime, 'to add new master');
            return res.send({ msg: 'Success!', master: master.toObject() });
        })        
    });
});

router.delete('/:masterId', function(req, res) {
    var errors = [];
    res.status(400);

    req.checkParams("masterId", "").isInt();
    if(req.validationErrors()) {
        return res.send();
    }
    
    Master.findById(req.params.masterId, function(err, master) {
        if(err || !master) {
            errors.push({msg: 'Could not find master to remove'});
            return res.send({ "errors": errors });
        }

        master.remove(function(err) {
            if(err) {
                errors.push({ msg: 'Server error when removing master from db.' });
                console.log(err);
                return res.send({ "errors": errors });             
            }
            
            res.status(200);
            return res.send({ msg: 'Success!' });
        })              
        
    });
});


module.exports = router;