var express = require('express');
var mongoose = require('mongoose');
var router = express.Router({mergeParams: true});
var bcrypt = require('bcrypt');

var Auth0 = require('auth0');
var User = require('../../models/user');

var AUTH0_DOMAIN = "foxula.eu.auth0.com";
var AUTH0_CLIENT_ID = "CsGH9x11X39EjHsIkjAONpZ17EcXke1k";

var apiEndpoint = 'https://' + AUTH0_DOMAIN + '/api/v2/';

var auth0 = new Auth0({
    domain: AUTH0_DOMAIN,
    clientID: AUTH0_CLIENT_ID,
    clientSecret: 'l-VQF7RBf-Rr7ewi9KXIIefkQR6XtSZaL7wGUgHL56pYX4n1CZgPVHSQrNQ49xon'
});


router.get('/', function(req, res) {
    
    auth0.getUsers({connection: 'Username-Password-Authentication'}, function (err, result) {
        if(err) {
            console.log('Could not get users: ' + err);
            return res.sendStatus(500);
        }
        
        res.status(200);
        return res.json(result);
    });
});


router.post('/', function(req, res) {
    console.log('Trying to create user');
    
    auth0.createUser({
        connection: 'Username-Password-Authentication',
        email: req.body.email,
        password: 'testPassword'
      }, function(err, profile, id_token) {
          if(err) {
              console.log('Signup failed!');
              console.log(err);
              return res.sendStatus(400);
          }
          console.log('User created');
          res.status(200);
          return res.json({user: profile, id_token: id_token});
      });
});
    
// --------------- //
// Update user
// --------------- //
// router.put('/:userId', function(req, res) {
//     var errors = [];
//     res.status(400);
    

// });

// --------------- //
// Delete user  
// --------------- //
router.delete('/:userId', function(req, res) {
    console.log('Trying to delete user. ');
    console.log('User with id ' + req.params.userId);
    
    auth0.deleteUser(req.params.userId, function(err) {
        if(err) {
            console.log(err);
            return res.sendStatus(500);
        }
        
        return res.sendStatus(200);
    });

});

module.exports = router;