var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.send('Api landing page');
});

router.use('/buses', require('./buses'));
router.use('/slaves', require('./slaves'));
router.use('/masters', require('./masters'));
router.use('/templogs', require('./tempLogs'));
router.use('/users', require('./users'));

module.exports = router;