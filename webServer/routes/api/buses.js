// TODO: ADD PROPER VALIDATION AS FOR THE SLAVES.js!!! Make sure a bus
// can not be added to an already occupied slave.

var express = require('express');
var mongoose = require('mongoose');
var router = express.Router({mergeParams: true});
var elapsedTime = require('../../utils/elapsedTime');

var tempLogRouter = require('./tempLogs');

var Bus = require('../../models/bus');
var Master = require('../../models/master');

var validationErrors = [];

router.use('/:busId/templogs', tempLogRouter);

// Fetch and display all buses from db
router.get('/', function(req, res) {
    var startTime = process.hrtime();
    console.log();
    console.log(req.params);
    console.log();
    var searchFilter = {};
    if(typeof req.params.slaveId !== 'undefined') searchFilter = { '_slave': req.params.slaveId };
    
    Bus
        .find(searchFilter)
        .exec(function(err, buses){
            
            if(err) {
                res.sendStatus(400);
                throw err;   
            }
            
            res.json(buses);
            elapsedTime(startTime, 'to fetch all buses');
        });
});

// TODO: Server side validation of regNr, maybe?
router.post('/', function(req, res) {
    var startTime = process.hrtime();
    var errors = [];
    res.status(400);
    
    var bus = new Bus();
    bus.registrationNumber = req.body.registrationNumber;
    bus.tempQuotient = req.body.tempQuotient || 1;
    bus.tempMin = req.body.tempMin || 20;
    bus.tempLogs = [];
    

    console.log('Saving bus to db');
    
    // Check if the bus should be added to a slave:
    var addSlaveReference = false;
    if(typeof req.params.slaveId !== 'undefined') {
        if(!mongoose.Types.ObjectId.isValid(req.params.slaveId)) {
            errors.push({ "msg": 'Invalid ObjectId for slave' });
            return res.send({"errors": errors});
        }
        bus._slave = req.params.slaveId;
        bus._id = mongoose.Types.ObjectId();
        addSlaveReference = true;
        console.log('Should add slave reference');
    }
    
    // TODO: Rewrite to disallow a bus to be added if the slave reference fails
    bus.save(function(err, savedBus) {
       if(err) {
            errors.push({ "msg": 'Server error when saving bus to db' });
            console.log(err);
            return res.send({"errors": errors});           
       } 
       

        if(addSlaveReference) {
            // Update the specified slave
            console.log('Adding slave reference to new bus. Master id: ' + req.params.masterId)
            Master.find({ '_id': req.params.masterId, 'slaves._id': req.params.slaveId }, function(err, masters) {
               if(err || masters.length < 1) {
                    errors.push({ "msg": 'Could not find the specified slave.' });
                    return res.send({"errors": errors});              
               } 
               
               console.log('Saved bus: ');
               console.log(savedBus);
               
               // Add the bus reference to the slave and save the changed slave.
               masters[0].slaves.id(req.params.slaveId)._bus = savedBus._id;

               masters[0].save(function(err, master) {
                    if(err) {
                        errors.push({ "msg": 'Could not update bus reference on slave' });
                        return res.send({"errors": errors});                          
                    }
               
                res.status(200);
                elapsedTime(startTime, 'to add new bus');
                return res.send({ msg: 'Success!', "bus": bus }); 
               })
             
            });
        }
        else {
            res.status(200);
            elapsedTime(startTime, 'to add new bus');
            return res.send({ msg: 'Success!', "bus": bus });    
        }
    });
});

// Update bus
router.put('/', function(req, res) {
    console.log('Updating bus');
    var errors = [];
    res.status(400);
    
    Bus.findById(req.body._id, function(err, bus) {
        if(err || !bus) {
            errors.push({ "msg": 'Could not find bus to change' });
            return res.send({"errors": errors});           
        }

        var updateSlaveBusRef = false;
        
        // Check if the bus already is connected to a slave and if 
        // the slave's _id is valid.
        if(typeof req.params.slaveId !== 'undefined') {
            console.log('Slave id specified');
            if(!mongoose.Types.ObjectId.isValid(req.params.slaveId)) {
                errors.push({ "msg": 'Invalid slave ID' });
                return res.send({"errors": errors});                  
            }
            
            // Validate slave reference and check if the ref should be changed
            if(typeof req.body._slave !== 'undefined' && mongoose.Types.ObjectId.isValid(req.body._slave)) {
                    
                console.log('Should update slave reference');
                if(req.body._slave != bus._slave) updateSlaveBusRef = true;
            }
            else {
                
                errors.push({ "msg": 'Invalid or unspecified slave ID.' });
                return res.send({"errors": errors});                 
            }
        }
    
        // TODO: Validate registration number and tempQuotient
        bus._slave = req.body._slave;
        bus.registrationNumber = req.body.registrationNumber;
        
        
        if(req.body.tempQuotient) bus.tempQuotient = req.body.tempQuotient;
        if(req.body.tempMin) bus.tempMin = req.body.tempMin;
        
        if(req.body.scheduleStart) bus.scheduleStart = req.body.scheduleStart;
        if(req.body.scheduleStop) bus.scheduleStop = req.body.scheduleStop;
        
        // Save bus and remove old slave reference
        if(updateSlaveBusRef) {
            
            console.log('Trying to find old slave with id:' + req.params.slaveId + ' and master: ' + req.params.masterId);
            
            //Find old slave
            Master.findOne({'_id': req.params.masterId, 'slaves._id': req.params.slaveId }, function(err, master) {
                if(err || !master) {
                    console.log('Error: ' + err);
                    console.log('Master: '+ master);
                    errors.push({ msg: 'Could not find old slave' });
                    return res.send({ "errors": errors });            
                }
                
                console.log('Removing old slave reference');
                
                // Remove old slave bus reference
                master.slaves.id(req.params.slaveId)._bus = null;
                console.log(master.slaves.id(req.params.slaveId));
                master.save(function(err) {
                    if(err) {
                        errors.push({ msg: 'Could not remove old slave reference' });
                        return res.send({ "errors": errors });                            
                    } 
                
                    updateBus(bus);

                });
            });
        }
        // Save bus without removing any old reference
        else {
            updateBus(bus);
        }
    });
    
    function updateBus(bus) {
        console.log('UpdateBus()');
        
        // If bus should be connected to a slave
        if(typeof bus._slave !== 'undefined' && bus._slave != null && bus._slave != -1 && !req.body.preventSlaveUpdate) {
            console.log('Bus should be connected to slave');
            
            // Update bus and slave    
            console.log('New slave id:' + bus._slave);
            console.log('New master id:' + req.body._master);
            Master.findOne({ '_id': req.body._master, 'slaves._id': bus._slave }, function(err, master) {
                if(err || !master) {
                    errors.push({ msg: 'Could not find new slave' });
                    return res.send({ "errors": errors });            
                }        
                
                master.slaves.id(bus._slave)._bus = bus._id;
                
                // Save slave
                master.save(function(err) {
                    if(err) {
                        errors.push({ msg: 'Server error. Could not add bus reference to the new slave' });
                        return res.send({ "errors": errors });                                 
                    }
                    
                    saveBusToDb(bus);
                });
            });
        }
        else {
            bus._slave = null;
            saveBusToDb(bus);
        }
    }
    
    function saveBusToDb(bus) {
                
        // Save the bus to db
        bus.save(function(err) {
            if(err){
                errors.push({ msg: 'Server error. Could save bus to db' });
                console.log(err);
                return res.send({ "errors": errors });                              
            }
            
            res.status(200);
            res.send({ msg: 'Success!' });
        })
    }
});


// Delete bus
// TODO: Radera bus-relation hos eventuell slav vid borttagande av bus
router.delete('/:busId', function(req, res) {
    var errors = [];
    res.status(400);
    
    var id;
    
    if(mongoose.Types.ObjectId.isValid(req.params.busId)) {
        id = mongoose.Types.ObjectId(req.params.busId);
    }
    else {
        validationErrors.push('Invalid ObjectId for bus');
        console.log('Invalid ObjectId for bus: ' + req.params.busId);
        return res.send({errors: validationErrors});
    }
    
    console.log('Deleting bus ' + req.params.busId);
    
    
    Bus.findById(id, function(err, bus) {
        if(err) {
            validationErrors.push('Server error occurred when trying to delete bus.');
            console.log('Could not delete bus: ' + err);
            return res.send({errors: validationErrors});           
        }      
        if(!bus) {
            validationErrors.push('Could not find bus to delete.');
            console.log('Could find bus to delete.');
            return res.send({errors: validationErrors});             
        }
        
        //Remove bus relation from related slave if existing
        if(bus._slave) {
            console.log('Removing slave bus relation. Slave id: ' + bus._slave);
            
            Master.findOne({ 'slaves._id': bus._slave }, function(err, master) {
                if(err) {
                    validationErrors.push('Server error occurred when trying to delete slave bus relation.');
                    console.log('Could not delete slave bus relation: ' + err);
                    return res.send({errors: validationErrors});           
                }      
                if(!master) {
                    validationErrors.push('Could not find master and slave to remove relation from.');
                    console.log('Could find slave to remove relation from.');
                    return res.send({errors: validationErrors});             
                }                
                
                master.slaves.id(bus._slave)._bus = null;
                
                master.save(function(err) {
                    if(err) {
                        errors.push('Error when updating slave');
                        console.log(err);
                        return res.send({"errors": errors});                           
                    }
                    
                    removeBus();
                })
            })
        }
        else {
            removeBus();
        }
    });


    
    
    function removeBus() {
        Bus.remove({ _id: id }, function(err, bus) {
            if(err) {
                validationErrors.push('Server error occurred when trying to delete bus.');
                console.log('Could not delete bus: ' + err);
                return res.send({errors: validationErrors});           
            }
            
            res.status(200);
            res.json({ message: 'Success!' });
        });        
    }
});



module.exports = router;