var express = require('express');
var mongoose = require('mongoose');
var router = express.Router({mergeParams: true});
var busRouter = require('./buses');

var Bus = require('../../models/bus');
var Slave = require('../../models/slave');
var Master = require('../../models/master');


router.use('/:slaveId/buses', busRouter);

// --------------- //
// Display all slaves
// --------------- //
router.get('/', function(req, res) {

    var searchFilter = {};
    if(typeof req.params.masterId !== 'undefined') searchFilter = { _id: req.params.masterId };
    
    Master
        .find(searchFilter)
        .populate('slaves._bus')
        .exec(function(err, masters) {
            if(err) {
                res.sendStatus(400);
                throw err;   
            }
            
            var slaves = [];
            
            // TODO: This isn't very effective? Keep it or not?
            for(var master of masters) {
                if(master.slaves.length > 0) {
                    for(var slave of master.slaves) {
                        slave = slave.toObject();
                        slave._master = master._id;
                        slaves.push(slave);
                    }
                }
            }
            
            res.json(slaves);    
            
        });
});


// -------------------- //
// Display slave by id
// -------------------- //
router.get('/:id', function(req, res) {
    var errors = [];
    
    Master
        .find({ 'slaves._id': req.params.id })
        .populate('slaves._bus')
        .exec(function(err, masters) {
            if(err) {
                errors.push({ msg: err.message });
                res.status(400);
                return res.json({ "errors": errors });
            }
            
            if(masters.length < 1) {
                return res.json([]);
            }
            
            var slave = masters[0].slaves.id(req.params.id).toObject();
            slave._master = masters[0]._id;
            
            return res.json(slave);    
            
        });
        
});

// Only accessible through /api/masters/:masterId/slaves as every slave needs
// to be connected to a master.
router.post('/', function(req, res) {
    var errors = [];
    res.status(400);
    
    var slave = new Slave();
    
    slave._id = mongoose.Types.ObjectId();
    slave.deviceId = req.body.deviceId;
    
    // Validate device id 
    req.checkBody("deviceId", "Enter a valid device ID").isInt();
    if(req.validationErrors()) {
        errors.push(req.validationErrors());
        return res.send({ "errors": errors });
    }

    
    Master.findById(req.params.masterId, function(err, master) {
        if(err || !master) {
            errors.push({ msg: "Could not find the specified master" });
            return res.send({"errors": errors}); 
        }
        
        var dupSlave = master.slaves.find(s => s.deviceId == req.body.deviceId);
        if(typeof dupSlave === 'undefined') {
            
            master.slaves.push(slave);
            
            master.save(function(err) {
                if(err) {
                    errors.push({ msg: 'Server error when saving slave to database.' });
                    return res.send({"errors": errors});                    
                }
                
                slave = slave.toObject();
                slave._master = req.params.masterId;
                
                res.status(200);
                return res.json({ message: 'Success!', "slave": slave });
                
            });
        }
        else {
            errors.push({ msg: 'Slave device ID not available for use.' });
            return res.send({"errors": errors}); 
        }
        
    });
});


    
// --------------- //
// Update slave
// --------------- //
router.put('/:slaveId', function(req, res) {
    var errors = [];
    res.status(400);
    
    // Validate master id
    if(typeof req.params.masterId === 'undefined') {
            errors.push({ msg: 'Unspecified master ID' });
            return res.send({"errors": errors});
    }

    Master.findById(req.params.masterId, function(err, master) {
        if(err) errors.push({ msg: err.message });
        if(!master) errors.push({ msg: 'Master could not be found' });
        if(errors.length > 0) return res.send({"errors": errors}); 
        
        // Validate slave ID
        if(typeof req.params.slaveId === 'undefined' || !mongoose.Types.ObjectId.isValid(req.params.slaveId)) {
            errors.push({ msg: 'Invalid ObjectId for slave' });
            return res.send({"errors": errors});           
        }
        
        // Make sure the slave exists
        var slaveToChange = master.slaves.id(req.params.slaveId);
        if(slaveToChange == null) {
            errors.push({ msg: 'Could not find slave.' });  
        }
        
        // Make sure the new device ID is not occupied already
        var dupSlave = master.slaves.find(s => s.deviceId == req.body.deviceId);
        if(typeof dupSlave !== 'undefined') {
            errors.push({ msg: 'Slave device ID already occupied.' });
        }
        
        if(errors.length > 0) return res.send({"errors": errors}); 
        
        // Update slave
        slaveToChange.deviceId = req.body.deviceId;
        
        master.save(function(err) {
            if(err) {
                errors.push({ msg: 'Could not update slave: ' + err.message });
                return res.send({"errors": errors});                     
            }
            
            res.status(200);
            return res.json({ message: 'Success!'});           
            
        });
    });    
});

// --------------- //
// Delete slave    
// --------------- //
router.delete('/:slaveId', function(req, res) {
    var errors = [];
    res.status(400);
    
    var id;
    
    if(typeof req.params.masterId === 'undefined') {
        errors.push('Invalid ObjectId for master');
        console.log('Invalid ObjectId for master: ' + req.params.masterId);
        return res.send({"errors": errors});        
    }
    
    // Validate object id
    if(typeof req.params.slaveId !== 'undefined' && mongoose.Types.ObjectId.isValid(req.params.slaveId)) {
        id = mongoose.Types.ObjectId(req.params.slaveId);    // TODO: Unnecessary conversion to object id
    }
    else {
        errors.push('Invalid ObjectId for slave');
        console.log('Invalid ObjectId for slave: ' + req.params.slaveId);
        return res.send({"errors": errors});
    }
    
    Master.findById(req.params.masterId, function(err, master) {
        if(err || !master) {
            errors.push('Could not find slave to delete');
            return res.send({"errors": errors});          
        }
        
        // Remove bus to slave relation if the slave has a connected bus
        if(master.slaves.id(req.params.slaveId)._bus) {
            Bus.findById(master.slaves.id(req.params.slaveId)._bus, function(err, bus) {
                if(err) {
                    errors.push('Could not find bus to remove slave relation from.');
                    console.log(err);
                    return res.send({"errors": errors});      
                }
                if(!bus) {
                    master.slaves.id(req.params.slaveId)._bus = null;
                    removeSlave(master);
                    return;
                }
                
                bus._slave = null;
                bus.save(function(err) {
                    if(err) {
                        errors.push('Error when updating bus');
                        console.log(err);
                        return res.send({"errors": errors});                           
                    }
                    
                    removeSlave(master);
                })
            })
        }
        else {
            removeSlave(master);
        }
    });
    
    function removeSlave(master) {
        // Remove slave from master 
        master.slaves.id(req.params.slaveId).remove(function(err) {
            if(err) {
                errors.push('Server error when removing slave from master');
                return res.send({"errors": errors});                  
            }
            
            master.save(function(err) {
                if(err) {
                    errors.push('Server error when saving updated master.');
                    return res.send({"errors": errors});                  
                }    
                
                res.status(200);
                return res.send({ msg: 'Success!' });
            });
        });        
    }
});

module.exports = router;