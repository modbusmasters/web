var express = require('express');
var mongoose = require('mongoose');
var moment = require('moment');
var router = express.Router({mergeParams: true});

var TempLog = require('../../models/tempLog');

// Fetch and display all buses from db
router.get('/', function(req, res) {
    var searchFilter = {};
    
    console.log('req.query:');
    console.log(req.query);
    if(req.query.from) {
        searchFilter.dateTime = { $gt: moment(req.query.from).toDate() };
        console.log('Fetching temp logs from:');
        console.log(moment(req.query.from).toDate());
    }
    
    if(typeof req.params.busId !== 'undefined' && mongoose.Types.ObjectId.isValid(req.params.busId)) searchFilter['_bus'] = req.params.busId;
    
    TempLog
        .find(searchFilter)
        .exec(function(err, tempLogs){
            
            if(err) {
                console.log(err);
                return res.sendStatus(400);
            }
            
            res.json(tempLogs);
        });
});

// Delete temp log
// router.delete('/:busId', function(req, res) {
//     var errors = [];
//     res.status(400);
// });



module.exports = router;