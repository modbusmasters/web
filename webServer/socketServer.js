"use strict";

var config = require('./config');
var util = require('util');
var moment = require('moment');
var socketIo = require('socket.io');
var net = require('net');
var mCom = require('./utils/modbusMasterCom.js');
var TempLogger = require('./utils/tempLogger.js');
var MasterSlaveBroadcaster = require('./utils/masterSlaveBroadcaster.js');
var Master = require('./models/master');

var socketioJwt = require('socketio-jwt');

/**
 * Handles socket.io and TCP socket connectivity to allow master communication
 * and data streaming to the frontend.
 * @module socketServer
 */
class SocketServer {

    constructor() {
        
        this.io = {};
        this.tcp = {};
        this.masterIdMap = {};  // Specifies relations between tcp socket ID and master ID
        this.streamList = {}; //List of slave ID's for active streams
    }

    startSocketServer(app) {
        
        this._initIoSocket(app);
        this.io.connectedClients = {};
        this.io.connectedClientsCount = 0;
        this._initTCPSocket();
        this.tcp.connectedClients = {};
    }

    // Setup Socket.IO server
    _initIoSocket(app) {
        
        this.io = socketIo.listen(app);
        
        // Authenticate incoming socket.io connections
        this.io.on('connection', socketioJwt.authorize({
            
            secret: Buffer(config['auth0Secret'], 'base64'),
            timeout: 10000 
            
        })).on('authenticated', function(socket) {
            
            //this socket is authenticated, we are good to handle more events from it.
            console.log('hello! ' + JSON.stringify(socket.decoded_token));
            this._ioOnConnection(socket);
        }.bind(this));

        console.log('Socket.io server started. Listening on port 80');
    }
    
    // Called every time a new socket.io client has connected and has successfully been authenticated.
    _ioOnConnection(socket) {
        console.log('a user connected');
    
        // Add connection to connectedClients list
        this.io.connectedClients[socket.id] = socket
        this.io.connectedClientsCount++;
        
        console.log('The new connection:');
        console.log(this.io.connectedClients[socket.id].id);
        
        // Send id back to client for future communication
        socket.emit('setId', { id: socket.id });

        // Receive slave and master ID used for the tcp communication
        socket.on('slaveConnect', this._ioOnSlaveConnect.bind(this));
        
        // Update relay states
        socket.on('sendSlaveData', this._ioOnSendSlaveData.bind(this));

        // Handle socket disconnection
        socket.on('disconnect', function() { this._ioOnDisconnect(socket) }.bind(this));   
    }
    
    _ioOnSlaveConnect(msg) {
        console.log('Slave data received for slave ' + msg.clientId);
        console.log(JSON.stringify(msg));
        
        console.log('Client id:' + msg.clientId);
        
        this.io.connectedClients[msg.clientId].data = msg.data;
        
        //Stream slave if it's not doing that already
        if(!this.streamList[msg.data.slaveDeviceId]) {
            
            console.log('Start streaming slave ' + msg.data.slaveDeviceId);
            this.streamList[msg.data.slaveDeviceId] = 1;
            
            for (var property in this.masterIdMap) {
                if (this.masterIdMap.hasOwnProperty(property)) {
                    if(property == msg.data.masterId) {
                        console.log('Sending initiate stream package to master ' + msg.data.masterId);
                        
                        mCom.initiateStream(this.tcp.connectedClients[this.masterIdMap[property]], msg.data.masterId, msg.data.slaveDeviceId);                            
                    }
                }
            }
        }
        else {
            this.streamList[msg.data.slaveDeviceId]++;
        }
    }
    
    _ioOnSendSlaveData(msg) {
        console.log('Send slave data');

        if(typeof this.tcp.connectedClients[this.masterIdMap[msg.data.masterId]] !== 'undefined') {
            
            // Send relay state to master
            mCom.setSlaveData(this.tcp.connectedClients[this.masterIdMap[msg.data.masterId]], msg.data.masterId, msg.data.slaveId, msg.data.operationMode, msg.data.relayStates, msg.data.temp.min, msg.data.temp.quotient, msg.data.schedule.start, msg.data.schedule.stop);
            
        }        
    }
    
    _ioOnDisconnect(socket) {
        console.log('Client ' + socket.id + ' disconnected');
        
        //Check if the stream should be removed;
        if(this.io.connectedClients[socket.id]) {
            var slaveId = this.io.connectedClients[socket.id].data.slaveDeviceId;
            this.streamList[slaveId]--;
            if(this.streamList[slaveId] <= 0) {
                console.log('Stop streaming slave ' + slaveId);
                delete this.streamList[slaveId];
                for (var property in this.tcp.connectedClients) {
                    if (this.tcp.connectedClients.hasOwnProperty(property)) {
                        mCom.stopStream(this.tcp.connectedClients[property], 1, slaveId); 
                    }
                }
            }            
        }

        
        delete this.io.connectedClients[socket.id];
        this.io.connectedClientsCount--;
    }

    // Setup TCP server
    _initTCPSocket() {
        this.tcp = net.createServer();
        this.tcp.listen(1337);
        console.log('TCP socket listening on 1337');

        //Create separate callback function!
        this.tcp.on('connection', function(sock) {

            console.log('TCP client connected: ' + sock.remoteAddress + ':' + sock.remotePort);
            
            sock.setTimeout(20000, function () {
                this._tcpCloseConnection(sock);
            }.bind(this));

            // On incoming data package
            sock.on('data', function(msg) {
                
                var incomingCmd = mCom.getCmdType(msg);
                var masterId = mCom.getMasterId(msg);
                var payload = mCom.getPayload(msg);
                
                switch(incomingCmd) {
                    case mCom.commands.sendMasterId:
                        console.log('Master ID received:  payload');
                        console.log('Saving master to active connections list');
                        
                        
                        if(!this.tcp.connectedClients[sock.id]) {
                            this._masterStatusUpdate(masterId, true);
                            this.tcp.connectedClients[sock.id] = sock;
                            this.masterIdMap[masterId] = sock.id;
                            
                            // Send list with all the master's slave IDs to the
                            // recently connected master:
                            var msb = new MasterSlaveBroadcaster(mCom);
                            msb.sendSlaveList(sock, masterId, function(success) {
                                console.log('Slavelist callback: ' + success);
                            });
                            
                            // Set timer to send server time to master 
                            this.tcp.connectedClients[sock.id].timeInterval = setInterval(function() {
                                var midnight = moment().startOf('day');
                                var seconds = moment().diff(midnight, 'seconds');
                                
                                mCom.setMasterData(sock, masterId, seconds);
                                
                            }.bind(this), 10000);   
                            
                            
                        }
                    
                    case mCom.commands.sendSlaveData:
                        // console.log('Slave data received');
                        this._tcpHandleSendSlaveData(masterId, payload);
                        break;
                }
            }.bind(this));
            
            // Remove client from connected list on disconnect
            sock.on('end', function() {
                
                this._tcpCloseConnection(sock);
                
            }.bind(this));
            
            sock.on('error', function(err) {
                
                if(err.code == 'ECONNRESET') {
                    console.log('Going to close connection');
                    this._tcpCloseConnection(sock);
                }
                
                console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
                console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
                console.log('Error!');
                console.log(util.inspect(err, { showHidden: true, depth: null }));
                console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
                console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            }.bind(this));
            
        }.bind(this));
    
        //Create separate callback function!
        this.tcp.on('error', function(err) {
            console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            console.log('Error!');
            console.log(util.inspect(err, { showHidden: true, depth: null }));
            console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
        });
    }
    
    _tcpCloseConnection(sock) {
                        
        console.log('client disconnected');
        if(typeof this.tcp.connectedClients[sock.id] !== 'undefined') {
            clearInterval(this.tcp.connectedClients[sock.id].timeInterval);
            delete this.tcp.connectedClients[sock.id];
            
            for (var property in this.masterIdMap) {
                if (this.masterIdMap.hasOwnProperty(property)) {
                    this.masterIdMap[property] == sock.id;
                    this._masterStatusUpdate(property, false);
                    
                    delete this.masterIdMap[property];
                }
            }
        }        
    }
    
    // Handle incoming slave data (temp values etc.)
    _tcpHandleSendSlaveData(masterId, payload) {
        
        var slaveId = parseInt(payload[0]);
        var streamClients = [];
        
        // Convert temp values to float
        var temp1Int = mCom.bytesToInt32(payload, 1);
        var temp2Int = mCom.bytesToInt32(payload, 5);
        
        var heatStatus = payload[10];
        
        var temp1 = mCom.toFloat(temp1Int);
        var temp2 = mCom.toFloat(temp2Int);
        
        // Check if someone is streaming the slave
        for (var prop in this.io.connectedClients) {
            if (this.io.connectedClients.hasOwnProperty(prop)) {
                if(typeof this.io.connectedClients[prop].data !== 'undefined' && this.io.connectedClients[prop].data.slaveDeviceId == slaveId) {
                    
                    // console.log('There is someone streaming the slave! :)');
                    
                    // Send temperatures to client
                    this.io.connectedClients[prop].emit('temp1', temp1);
                    this.io.connectedClients[prop].emit('temp2', temp2);
                    this.io.connectedClients[prop].emit('heatStatus', heatStatus);
                }
                else {
                    // console.log('No active stream for slave');
                }
            }
        }
        
        console.log('Payload 11:' + payload[11]);
        //Only save messages with the log flag set
        if(payload[11]) {
            var tempLogger = new TempLogger(masterId, slaveId);
            
            tempLogger.push(temp2.toFixed(1), temp1.toFixed(1), function(success) {
                if(success) {
                    console.log('Templog added successfully');
                    console.log('Connected clients: ' + this.io.connectedClientsCount);
                }
                else {
                    console.log('Templog could not be added');
                }
            }.bind(this));                 
        }
    }
    
    //Update online status for master in database
    _masterStatusUpdate(masterId, online) {
        console.log('Master status update for ' + masterId);
        Master.update({_id: masterId}, {$set: { online: online }}, function(err) {
            if(err) throw err;           
        });
        
    }
}

module.exports = SocketServer;