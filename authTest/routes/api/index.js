var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var User = require('../../models/user');

router.get('/', function(req, res) {
    res.json({'message': "Modbus master API landing page"});
});

router.use('/users', require('./users/index'));

module.exports = router;