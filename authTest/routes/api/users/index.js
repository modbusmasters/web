var express = require('express');
var router = express.Router();
var User = require('../../../models/user');

// List all users
router.get('/', function(req, res) {
    User.find({}, function(err, users) {
        if(err) {
            res.status(500);
            res.json({errors: ['Internal server error']});
        }
        
        res.json(users); 
    });
});

module.exports = router;