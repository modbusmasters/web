var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var User = require('../models/user');
var jwt = require('jsonwebtoken');
var config = require('../config');

router.post('/', function(req, res) {
    console.log('Username: ' + req.body.username);
    User.findOne({
        username: req.body.username
    }, function(err, user) {

        if(err) throw err;
        
        if(!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        }
        else if(user) {
            
            var hash = bcrypt.hashSync(req.body.password, user.salt);
            
            if(user.password != hash) {
                res.json({ success: false, message: 'Authentication failed. Wrong password.' });
            }
            else {
                
                //Create token
                var tokenPub = {
                    _id: user._id,
                    username: user.username
                };

                var token = jwt.sign(tokenPub, config.secret, {
                   expiresIn: 1200  //Seconds
                });
                
                res.json({
                    success: true,
                    message: 'Enjoy your token!',
                    token: token
                });
            }
        }
        
    });
    
});


module.exports = router;