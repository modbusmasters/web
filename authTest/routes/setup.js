var express = require('express');
var router = express.Router();
var User = require('../models/user');
var bcrypt = require('bcrypt');

/* Setup default testUser */
router.get('/', function(req, res, next) {
    console.log('Got /setup request');
    User.find({
        username: 'testUser'
    }, function(err, user) {
        if (err || !user.length) {
            console.log('testUser not found, creating him/her');
            
            var salt = bcrypt.genSaltSync(10);
            var hash = bcrypt.hashSync("abc123", salt);

            var testUser = new User({
                username: 'testUser',
                password: hash,
                salt: salt
            })

            console.log('Password hash: ' + testUser.password);
            console.log('saving user...');

            testUser.save(function(err) {
                if (err) throw err;

                console.log('User saved successfully');
                res.json({
                    success: true
                });
            });
        }
        else {
            console.log('testUser already exists');
            console.log(user);
            res.json({
                success: false
            });
        }
    });

});

module.exports = router;
