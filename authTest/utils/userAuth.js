var jwt = require('jsonwebtoken');
var config = require('../config');

module.exports = function(req, res, next) {
    
    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    
    if(token) {
        
        jwt.verify(token, config.secret, function(err, decoded) {
            if(err) {
               return res.json({ success: false, message: 'Failed to authenticate token.' });
            }
            else {

                //Save to request for future use
                req.decoded = decoded;
                next();
                
            }
        });
    }
    else {
        
        // Return error if no token
        return res.status(403).send({ 
            success: false, 
            message: 'No token provided.' 
        });        
        
    }
    
}